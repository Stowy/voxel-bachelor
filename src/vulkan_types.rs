use crate::vulkan_descriptors::DescriptorAllocatorGrowable;
use ash::vk;

#[repr(C)]
#[derive(Clone, Copy, bytemuck::Pod, bytemuck::Zeroable, Default)]
pub struct Vertex {
    pub position: na::Point3<f32>,
    pub uv_x: f32,
    pub normal: na::Vector3<f32>,
    pub uv_y: f32,
    pub color: na::Vector4<f32>,
}

#[repr(C)]
#[derive(Clone, Copy, bytemuck::Pod, bytemuck::Zeroable, Default)]
pub struct GpuDrawPushConstants {
    pub world_matrix: glm::Mat4,
    pub vertex_buffer_address: vk::DeviceAddress,
}

#[repr(C)]
#[derive(Clone, Copy, bytemuck::Pod, bytemuck::Zeroable, Default)]
pub struct ComputePushConstants {
    pub data1: glm::Vec4,
    pub data2: glm::Vec4,
    pub data3: glm::Vec4,
    pub data4: glm::Vec4,
}

pub struct ComputeEffect {
    pub name: String,
    pub pipeline: vk::Pipeline,
    pub layout: vk::PipelineLayout,
    pub data: ComputePushConstants,
}

pub struct AllocatedBuffer {
    pub buffer: vk::Buffer,
    pub allocation: vk_mem::Allocation,
    pub allocation_info: vk_mem::AllocationInfo,
}

#[derive(Default)]
pub struct FrameData {
    pub command_pool: vk::CommandPool,
    pub command_buffer: vk::CommandBuffer,
    pub swapchain_semaphore: vk::Semaphore,
    pub render_semaphore: vk::Semaphore,
    pub render_fence: vk::Fence,
    pub frame_descriptors: DescriptorAllocatorGrowable,
}

#[derive(Clone)]
pub struct GpuSceneData {
    pub view: na::Isometry3<f32>,
    pub projection: na::Perspective3<f32>,
    pub view_projection: na::Matrix4<f32>,
}

impl GpuSceneData {
    pub fn from_view_proj(view: na::Isometry3<f32>, projection: na::Perspective3<f32>) -> Self {
        let view_projection = projection.into_inner() * view.to_homogeneous();
        Self {
            view,
            projection,
            view_projection,
        }
    }
}
