use anyhow::Result;
use ash::vk;

pub struct DescriptorLayoutBuilder<'a> {
    bindings: Vec<vk::DescriptorSetLayoutBinding<'a>>,
}

impl DescriptorLayoutBuilder<'_> {
    pub fn new() -> Self {
        Self {
            bindings: Vec::new(),
        }
    }

    pub fn add_binding(&mut self, binding: u32, descriptor_type: vk::DescriptorType) {
        let new_bind = vk::DescriptorSetLayoutBinding::default()
            .binding(binding)
            .descriptor_count(1)
            .descriptor_type(descriptor_type);

        self.bindings.push(new_bind);
    }

    pub fn clear(&mut self) {
        self.bindings.clear();
    }

    pub unsafe fn build(
        &mut self,
        device: &ash::Device,
        shader_stages: vk::ShaderStageFlags,
    ) -> Result<vk::DescriptorSetLayout> {
        self.bindings
            .iter_mut()
            .for_each(|b| b.stage_flags |= shader_stages);

        let info = vk::DescriptorSetLayoutCreateInfo::default()
            .bindings(&self.bindings)
            .flags(vk::DescriptorSetLayoutCreateFlags::empty());

        let set = device.create_descriptor_set_layout(&info, None)?;

        Ok(set)
    }
}

#[derive(Copy, Clone)]
pub struct PoolSizeRatio {
    descriptor_type: vk::DescriptorType,
    ratio: f32,
}

impl PoolSizeRatio {
    pub fn new(descriptor_type: vk::DescriptorType, ratio: f32) -> Self {
        Self {
            descriptor_type,
            ratio,
        }
    }
}

pub struct DescriptorAllocator {
    pool: vk::DescriptorPool,
}

impl DescriptorAllocator {
    pub unsafe fn new(
        device: &ash::Device,
        max_sets: u32,
        pool_ratios: &[PoolSizeRatio],
    ) -> Result<Self> {
        let pool_sizes = pool_ratios
            .iter()
            .map(|ratio| {
                vk::DescriptorPoolSize::default()
                    .ty(ratio.descriptor_type)
                    .descriptor_count((ratio.ratio * max_sets as f32) as u32)
            })
            .collect::<Vec<_>>();

        let pool_info = vk::DescriptorPoolCreateInfo::default()
            .max_sets(max_sets)
            .pool_sizes(&pool_sizes);

        let pool = device.create_descriptor_pool(&pool_info, None)?;

        Ok(Self { pool })
    }

    pub unsafe fn clear_descriptors(&self, device: &ash::Device) -> Result<()> {
        device.reset_descriptor_pool(self.pool, vk::DescriptorPoolResetFlags::empty())?;

        Ok(())
    }

    pub unsafe fn destroy_pool(&self, device: &ash::Device) {
        device.destroy_descriptor_pool(self.pool, None);
    }

    pub unsafe fn allocate(
        &self,
        device: &ash::Device,
        layout: vk::DescriptorSetLayout,
    ) -> Result<vk::DescriptorSet> {
        let layouts = &[layout];
        let alloc_info = vk::DescriptorSetAllocateInfo::default()
            .descriptor_pool(self.pool)
            .set_layouts(layouts);

        let descriptor_set = device.allocate_descriptor_sets(&alloc_info)?[0];
        Ok(descriptor_set)
    }
}

const MAX_SETS_PER_POOL: u32 = 4092;

#[derive(Default)]
pub struct DescriptorAllocatorGrowable {
    ratios: Vec<PoolSizeRatio>,
    full_pools: Vec<vk::DescriptorPool>,
    ready_pools: Vec<vk::DescriptorPool>,
    sets_per_pool: u32,
}

impl DescriptorAllocatorGrowable {
    pub fn new(
        device: &ash::Device,
        initial_sets: u32,
        pool_ratios: &[PoolSizeRatio],
    ) -> Result<Self> {
        let ratios = pool_ratios.to_vec();

        let new_pool = Self::create_pool(device, initial_sets, &ratios)?;
        let sets_per_pool = initial_sets + initial_sets / 2;
        let ready_pools = vec![new_pool];

        Ok(Self {
            ratios,
            ready_pools,
            sets_per_pool,
            full_pools: Vec::new(),
        })
    }

    pub unsafe fn clear_pools(&mut self, device: &ash::Device) -> Result<()> {
        for pool in &self.ready_pools {
            device.reset_descriptor_pool(*pool, vk::DescriptorPoolResetFlags::empty())?;
        }

        for pool in &self.full_pools {
            device.reset_descriptor_pool(*pool, vk::DescriptorPoolResetFlags::empty())?;
            self.ready_pools.push(*pool);
        }

        self.full_pools.clear();

        Ok(())
    }

    pub unsafe fn destroy_pools(&mut self, device: &ash::Device) {
        for pool in &self.ready_pools {
            device.destroy_descriptor_pool(*pool, None);
        }

        self.ready_pools.clear();

        for pool in &self.full_pools {
            device.destroy_descriptor_pool(*pool, None);
        }

        self.full_pools.clear();
    }

    pub fn allocate(
        &mut self,
        device: &ash::Device,
        layout: vk::DescriptorSetLayout,
    ) -> Result<vk::DescriptorSet> {
        let mut pool_to_use = self.get_pool(device)?;

        let mut alloc_info = vk::DescriptorSetAllocateInfo::default()
            .descriptor_pool(pool_to_use)
            .set_layouts(std::slice::from_ref(&layout));

        let result = unsafe { device.allocate_descriptor_sets(&alloc_info) };

        let descriptor_set = if result == Err(vk::Result::ERROR_OUT_OF_POOL_MEMORY)
            || result == Err(vk::Result::ERROR_FRAGMENTED_POOL)
        {
            self.full_pools.push(pool_to_use);
            pool_to_use = self.get_pool(device)?;
            alloc_info.descriptor_pool = pool_to_use;
            unsafe { device.allocate_descriptor_sets(&alloc_info)?[0] }
        } else {
            result?[0]
        };

        self.ready_pools.push(pool_to_use);

        Ok(descriptor_set)
    }

    pub fn get_pool(&mut self, device: &ash::Device) -> Result<vk::DescriptorPool> {
        if !self.ready_pools.is_empty() {
            Ok(self.ready_pools.pop().unwrap())
        } else {
            let new_pool = Self::create_pool(device, self.sets_per_pool, &self.ratios)?;

            self.sets_per_pool += self.sets_per_pool / 2;
            self.sets_per_pool = self.sets_per_pool.min(MAX_SETS_PER_POOL);

            Ok(new_pool)
        }
    }

    pub fn create_pool(
        device: &ash::Device,
        set_count: u32,
        pool_ratios: &[PoolSizeRatio],
    ) -> Result<vk::DescriptorPool> {
        let pool_sizes: Vec<vk::DescriptorPoolSize> = pool_ratios
            .iter()
            .map(|r| {
                vk::DescriptorPoolSize::default()
                    .ty(r.descriptor_type)
                    .descriptor_count((r.ratio * set_count as f32) as u32)
            })
            .collect();

        let pool_info = vk::DescriptorPoolCreateInfo::default()
            .max_sets(set_count)
            .pool_sizes(&pool_sizes);

        let new_pool = unsafe { device.create_descriptor_pool(&pool_info, None)? };
        Ok(new_pool)
    }
}

enum WriteInfoPointer {
    Buffer(usize),
    Image(usize),
}

// This struct is necessary because we can't directly store a vk::WriteDescriptorSet
// since it requires a pointer to a vk::DescriptorImageInfo or vk::DescriptorBufferInfo.
// To solve this, we store an index to it and build the WriteDescriptorSets in the update_set function.
struct WriteInfo {
    dst_binding: u32,
    descriptor_type: vk::DescriptorType,
    write_info_pointer: WriteInfoPointer,
}

pub struct DescriptorWriter {
    image_infos: Vec<vk::DescriptorImageInfo>,
    buffer_infos: Vec<vk::DescriptorBufferInfo>,
    writes: Vec<WriteInfo>,
}

impl DescriptorWriter {
    pub fn new() -> Self {
        Self {
            image_infos: Vec::new(),
            buffer_infos: Vec::new(),
            writes: Vec::new(),
        }
    }

    pub fn write_image(
        &mut self,
        binding: u32,
        image: vk::ImageView,
        sampler: vk::Sampler,
        layout: vk::ImageLayout,
        descriptor_type: vk::DescriptorType,
    ) {
        let image_info = vk::DescriptorImageInfo::default()
            .sampler(sampler)
            .image_view(image)
            .image_layout(layout);

        self.image_infos.push(image_info);

        let image_info_index = self.image_infos.len() - 1;

        let write_info = WriteInfo {
            write_info_pointer: WriteInfoPointer::Image(image_info_index),
            dst_binding: binding,
            descriptor_type,
        };

        self.writes.push(write_info);
    }

    pub fn write_buffer(
        &mut self,
        binding: u32,
        buffer: vk::Buffer,
        size: vk::DeviceSize,
        offset: vk::DeviceSize,
        descriptor_type: vk::DescriptorType,
    ) {
        let buffer_info = vk::DescriptorBufferInfo::default()
            .buffer(buffer)
            .offset(offset)
            .range(size);
        self.buffer_infos.push(buffer_info);

        let buffer_index = self.buffer_infos.len() - 1;

        let write_info = WriteInfo {
            write_info_pointer: WriteInfoPointer::Buffer(buffer_index),
            dst_binding: binding,
            descriptor_type,
        };

        self.writes.push(write_info);
    }

    pub fn clear(&mut self) {
        self.image_infos.clear();
        self.writes.clear();
        self.buffer_infos.clear();
    }

    pub fn update_set(&mut self, device: &ash::Device, set: vk::DescriptorSet) {
        for write in &self.writes {
            let write_descriptor_set = vk::WriteDescriptorSet::default()
                .dst_binding(write.dst_binding)
                .dst_set(set)
                .descriptor_type(write.descriptor_type);

            let write_descriptor_set = match write.write_info_pointer {
                WriteInfoPointer::Buffer(index) => write_descriptor_set
                    .buffer_info(std::slice::from_ref(&self.buffer_infos[index])),
                WriteInfoPointer::Image(index) => {
                    write_descriptor_set.image_info(std::slice::from_ref(&self.image_infos[index]))
                }
            };

            unsafe {
                device.update_descriptor_sets(std::slice::from_ref(&write_descriptor_set), &[])
            };
        }
    }
}
