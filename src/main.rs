#![allow(dead_code)]

#[macro_use]
extern crate log;
extern crate nalgebra as na;
extern crate nalgebra_glm as glm;
extern crate pretty_env_logger;
extern crate vk_mem;

use anyhow::Result;
use winit::event_loop::{ControlFlow, EventLoop};

use vulkan_engine::VulkanEngine;

mod imgui_rs_vulkan_renderer;
mod nalgebra_utils;
mod vulkan_descriptors;
mod vulkan_engine;
mod vulkan_pipelines;
mod vulkan_types;
mod vulkan_utils;

fn main() -> Result<()> {
    pretty_env_logger::init();

    let event_loop = EventLoop::new().unwrap();
    event_loop.set_control_flow(ControlFlow::Poll);

    let mut engine = VulkanEngine::default();

    event_loop
        .run_app(&mut engine)
        .expect("Error while running the event loop.");

    Ok(())
}
