use anyhow::{anyhow, Result};
use ash::vk;
use std::ffi::CStr;
use std::fs::File;
use std::io;
use std::io::Read;
use std::path::Path;

#[derive(Default)]
pub struct PipelineBuilder<'a> {
    pub pipeline_layout: vk::PipelineLayout,
    shader_stages: Vec<vk::PipelineShaderStageCreateInfo<'a>>,
    input_assembly: vk::PipelineInputAssemblyStateCreateInfo<'a>,
    rasterizer: vk::PipelineRasterizationStateCreateInfo<'a>,
    color_blend_attachment: vk::PipelineColorBlendAttachmentState,
    multisampling: vk::PipelineMultisampleStateCreateInfo<'a>,
    depth_stencil: vk::PipelineDepthStencilStateCreateInfo<'a>,
    render_info: vk::PipelineRenderingCreateInfo<'a>,
    color_attachment_format: vk::Format,
}

impl PipelineBuilder<'_> {
    pub fn new() -> Self {
        Self {
            ..Default::default()
        }
    }

    pub fn build_pipeline(&mut self, device: &ash::Device) -> Result<vk::Pipeline> {
        let viewport_state = vk::PipelineViewportStateCreateInfo::default()
            .viewport_count(1)
            .scissor_count(1);

        let color_blending = vk::PipelineColorBlendStateCreateInfo::default()
            .logic_op_enable(false)
            .logic_op(vk::LogicOp::COPY)
            .attachments(std::slice::from_ref(&self.color_blend_attachment));

        let vertex_input_info = vk::PipelineVertexInputStateCreateInfo::default();

        let state = [vk::DynamicState::VIEWPORT, vk::DynamicState::SCISSOR];
        let dynamic_info = vk::PipelineDynamicStateCreateInfo::default().dynamic_states(&state);

        let pipeline_info = vk::GraphicsPipelineCreateInfo::default()
            .push_next(&mut self.render_info)
            .stages(&self.shader_stages)
            .vertex_input_state(&vertex_input_info)
            .input_assembly_state(&self.input_assembly)
            .viewport_state(&viewport_state)
            .rasterization_state(&self.rasterizer)
            .multisample_state(&self.multisampling)
            .color_blend_state(&color_blending)
            .depth_stencil_state(&self.depth_stencil)
            .layout(self.pipeline_layout)
            .dynamic_state(&dynamic_info);

        let pipeline = unsafe {
            device
                .create_graphics_pipelines(
                    vk::PipelineCache::null(),
                    std::slice::from_ref(&pipeline_info),
                    None,
                )
                .map_err(|e| e.1)?[0]
        };

        Ok(pipeline)
    }

    pub fn set_shaders(
        &mut self,
        vertex_shader: vk::ShaderModule,
        fragment_shader: vk::ShaderModule,
    ) {
        self.shader_stages.clear();
        let entry = CStr::from_bytes_until_nul(b"main\0").unwrap();

        self.shader_stages.push(
            vk::PipelineShaderStageCreateInfo::default()
                .stage(vk::ShaderStageFlags::VERTEX)
                .name(entry)
                .module(vertex_shader),
        );

        self.shader_stages.push(
            vk::PipelineShaderStageCreateInfo::default()
                .stage(vk::ShaderStageFlags::FRAGMENT)
                .name(entry)
                .module(fragment_shader),
        );
    }

    pub fn set_input_topology(&mut self, topology: vk::PrimitiveTopology) {
        self.input_assembly.topology = topology;
        self.input_assembly.primitive_restart_enable = vk::FALSE;
    }

    pub fn set_polygon_mode(&mut self, mode: vk::PolygonMode) {
        self.rasterizer.polygon_mode = mode;
        self.rasterizer.line_width = 1.0;
    }

    pub fn set_cull_mode(&mut self, cull_mode: vk::CullModeFlags, front_face: vk::FrontFace) {
        self.rasterizer.cull_mode = cull_mode;
        self.rasterizer.front_face = front_face;
    }

    pub fn set_multisampling_none(&mut self) {
        self.multisampling.sample_shading_enable = vk::FALSE;
        self.multisampling.rasterization_samples = vk::SampleCountFlags::TYPE_1;
        self.multisampling.min_sample_shading = 1.0;
        self.multisampling.p_sample_mask = std::ptr::null();
        self.multisampling.alpha_to_coverage_enable = vk::FALSE;
        self.multisampling.alpha_to_one_enable = vk::FALSE;
    }

    pub fn disable_blending(&mut self) {
        self.color_blend_attachment.color_write_mask = vk::ColorComponentFlags::RGBA;
        self.color_blend_attachment.blend_enable = vk::FALSE;
    }

    pub fn set_color_attachment_format(&mut self, format: vk::Format) {
        self.color_attachment_format = format;
        self.render_info.color_attachment_count = 1;
        self.render_info.p_color_attachment_formats = &self.color_attachment_format;
    }

    pub fn set_depth_format(&mut self, format: vk::Format) {
        self.render_info.depth_attachment_format = format;
    }

    pub fn enable_depth_test(&mut self, depth_write_enable: bool, op: vk::CompareOp) {
        self.depth_stencil.depth_test_enable = vk::TRUE;
        self.depth_stencil.depth_write_enable = vk::Bool32::from(depth_write_enable);
        self.depth_stencil.depth_compare_op = op;
        self.depth_stencil.depth_bounds_test_enable = vk::FALSE;
        self.depth_stencil.stencil_test_enable = vk::FALSE;
        self.depth_stencil.front = vk::StencilOpState::default();
        self.depth_stencil.back = vk::StencilOpState::default();
        self.depth_stencil.min_depth_bounds = 0.0;
        self.depth_stencil.max_depth_bounds = 1.0;
    }

    pub fn disable_depth_test(&mut self) {
        self.depth_stencil.depth_test_enable = vk::FALSE;
        self.depth_stencil.depth_write_enable = vk::FALSE;
        self.depth_stencil.depth_compare_op = vk::CompareOp::NEVER;
        self.depth_stencil.depth_bounds_test_enable = vk::FALSE;
        self.depth_stencil.stencil_test_enable = vk::FALSE;
        self.depth_stencil.front = vk::StencilOpState::default();
        self.depth_stencil.back = vk::StencilOpState::default();
        self.depth_stencil.min_depth_bounds = 0.0;
        self.depth_stencil.max_depth_bounds = 1.0;
    }

    pub fn enable_blending_additive(&mut self) {
        self.color_blend_attachment.color_write_mask = vk::ColorComponentFlags::RGBA;
        self.color_blend_attachment.blend_enable = vk::TRUE;
        self.color_blend_attachment.src_color_blend_factor = vk::BlendFactor::ONE;
        self.color_blend_attachment.dst_color_blend_factor = vk::BlendFactor::DST_ALPHA;
        self.color_blend_attachment.color_blend_op = vk::BlendOp::ADD;
        self.color_blend_attachment.src_alpha_blend_factor = vk::BlendFactor::ONE;
        self.color_blend_attachment.dst_alpha_blend_factor = vk::BlendFactor::ZERO;
        self.color_blend_attachment.alpha_blend_op = vk::BlendOp::ADD;
    }

    pub fn enabled_blending_alpha_blend(&mut self) {
        self.color_blend_attachment.color_write_mask = vk::ColorComponentFlags::RGBA;
        self.color_blend_attachment.blend_enable = vk::TRUE;
        self.color_blend_attachment.src_color_blend_factor = vk::BlendFactor::ONE_MINUS_DST_ALPHA;
        self.color_blend_attachment.dst_color_blend_factor = vk::BlendFactor::DST_ALPHA;
        self.color_blend_attachment.color_blend_op = vk::BlendOp::ADD;
        self.color_blend_attachment.src_alpha_blend_factor = vk::BlendFactor::ONE;
        self.color_blend_attachment.dst_alpha_blend_factor = vk::BlendFactor::ZERO;
        self.color_blend_attachment.alpha_blend_op = vk::BlendOp::ADD;
    }

    pub fn clear(&mut self) {
        self.input_assembly = Default::default();
        self.rasterizer = Default::default();
        self.color_blend_attachment = Default::default();
        self.multisampling = Default::default();
        self.pipeline_layout = Default::default();
        self.depth_stencil = Default::default();
        self.render_info = Default::default();
        self.shader_stages.clear();
    }
}

pub unsafe fn load_shader_module_from_bytes(
    buffer: &[u8],
    device: &ash::Device,
) -> Result<vk::ShaderModule> {
    if buffer.len() % 4 != 0 {
        return Err(anyhow!(io::Error::new(
            io::ErrorKind::InvalidData,
            "Shader module is of an invalid file size"
        )));
    }

    // SPIRV expects an u32 buffer, so we have to convert it first
    let content: Vec<u32> = buffer
        .chunks_exact(4)
        .map(|chunk| {
            let mut array = [0u8; 4];
            array.copy_from_slice(chunk);
            u32::from_le_bytes(array)
        })
        .collect();

    let create_info = vk::ShaderModuleCreateInfo::default().code(&content);

    let shader_module = device.create_shader_module(&create_info, None)?;

    Ok(shader_module)
}

pub unsafe fn load_shader_module(
    file_path: &Path,
    device: &ash::Device,
) -> Result<vk::ShaderModule> {
    let mut file = File::open(file_path)?;
    let mut buffer = Vec::new();
    file.read_to_end(&mut buffer)?;

    load_shader_module_from_bytes(&buffer, device)
}
