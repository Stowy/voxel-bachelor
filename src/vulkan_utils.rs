#![allow(deprecated)]

use anyhow::{anyhow, Result};
use ash::vk;
use ash::vk::Offset3D;
use raw_window_handle::HasRawDisplayHandle;
use std::collections::HashSet;
use std::ffi::{c_char, CStr, CString};
use std::os::raw::c_void;
use thiserror::Error;
use winit::window::Window;

const REQUIRED_LAYERS: [&str; 1] = ["VK_LAYER_KHRONOS_validation"];

#[derive(Debug, Error)]
#[error("Missing {0}.")]
pub struct SuitabilityError(pub &'static str);

#[derive(Debug)]
pub struct AllocatedImage {
    pub image: vk::Image,
    pub image_view: vk::ImageView,
    pub gpu_allocation: vk_mem::Allocation,
    pub image_extent: vk::Extent3D,
    pub image_format: vk::Format,
}

pub unsafe fn create_command_pool(
    instance: &ash::Instance,
    device: &ash::Device,
    physical_device: vk::PhysicalDevice,
) -> Result<vk::CommandPool> {
    let indices = QueueFamilyIndices::get(instance, physical_device)?;

    let info = vk::CommandPoolCreateInfo::default()
        .flags(vk::CommandPoolCreateFlags::RESET_COMMAND_BUFFER)
        .queue_family_index(indices.graphics);

    Ok(device.create_command_pool(&info, None)?)
}

pub fn get_swapchain_surface_format(formats: &[vk::SurfaceFormatKHR]) -> vk::SurfaceFormatKHR {
    formats
        .iter()
        .cloned()
        .find(|f| f.format == vk::Format::B8G8R8A8_UNORM)
        .unwrap_or_else(|| formats[0])
}

pub fn get_swapchain_present_mode(present_modes: &[vk::PresentModeKHR]) -> vk::PresentModeKHR {
    present_modes
        .iter()
        .cloned()
        .find(|m| *m == vk::PresentModeKHR::MAILBOX)
        .unwrap_or(vk::PresentModeKHR::FIFO)
}

pub fn get_swapchain_extent(
    window: &Window,
    capabilities: vk::SurfaceCapabilitiesKHR,
) -> vk::Extent2D {
    if capabilities.current_extent.width != u32::MAX {
        capabilities.current_extent
    } else {
        let size = window.inner_size();
        vk::Extent2D::default()
            .width(size.width.clamp(
                capabilities.min_image_extent.width,
                capabilities.max_image_extent.width,
            ))
            .height(size.height.clamp(
                capabilities.min_image_extent.height,
                capabilities.max_image_extent.height,
            ))
    }
}

pub unsafe fn create_swapchain_image_views(
    device: &ash::Device,
    swapchain_images: &[vk::Image],
    swapchain_format: vk::Format,
) -> Result<Vec<vk::ImageView>> {
    Ok(swapchain_images
        .iter()
        .map(|i| {
            let components = vk::ComponentMapping::default()
                .r(vk::ComponentSwizzle::IDENTITY)
                .g(vk::ComponentSwizzle::IDENTITY)
                .b(vk::ComponentSwizzle::IDENTITY)
                .a(vk::ComponentSwizzle::IDENTITY);

            let subresource_range = vk::ImageSubresourceRange::default()
                .aspect_mask(vk::ImageAspectFlags::COLOR)
                .base_mip_level(0)
                .level_count(1)
                .base_array_layer(0)
                .layer_count(1);

            let info = vk::ImageViewCreateInfo::default()
                .image(*i)
                .view_type(vk::ImageViewType::TYPE_2D)
                .format(swapchain_format)
                .components(components)
                .subresource_range(subresource_range);

            device.create_image_view(&info, None)
        })
        .collect::<Result<Vec<_>, _>>()?)
}

pub unsafe fn create_instance(window: &Window, entry: &ash::Entry) -> Result<ash::Instance> {
    let application_info = vk::ApplicationInfo::default()
        .application_name(CStr::from_bytes_with_nul_unchecked(b"Vulkan Guide\0"))
        .application_version(vk::make_api_version(0, 1, 0, 0))
        .engine_name(CStr::from_bytes_with_nul_unchecked(b"No Engine\0"))
        .engine_version(vk::make_api_version(0, 1, 0, 0))
        .api_version(vk::make_api_version(0, 1, 3, 0));

    let (_layer_names, layer_names_ptr) = get_layer_names_and_pointers();

    let layers = if crate::vulkan_engine::VALIDATION_ENABLED {
        check_validation_layer_support(entry);
        layer_names_ptr
    } else {
        Vec::new()
    };

    let create_flags = if cfg!(any(target_os = "macos", target_os = "ios")) {
        vk::InstanceCreateFlags::ENUMERATE_PORTABILITY_KHR
    } else {
        vk::InstanceCreateFlags::default()
    };

    let extensions = ash_window::enumerate_required_extensions(window.raw_display_handle()?)?;
    let mut extensions = Vec::from(extensions);
    if crate::vulkan_engine::VALIDATION_ENABLED {
        extensions.push(ash::ext::debug_utils::NAME.as_ptr());
    }

    let info = vk::InstanceCreateInfo::default()
        .application_info(&application_info)
        .enabled_layer_names(&layers)
        .enabled_extension_names(&extensions)
        .flags(create_flags);

    let instance = entry.create_instance(&info, None)?;

    Ok(instance)
}

/// Check if the required validation set in `REQUIRED_LAYERS`
/// are supported by the Vulkan instance.
///
/// # Panics
///
/// Panic if at least one on the layer is not supported.
pub unsafe fn check_validation_layer_support(entry: &ash::Entry) {
    for required in REQUIRED_LAYERS.iter() {
        let found = entry
            .enumerate_instance_layer_properties()
            .unwrap()
            .iter()
            .any(|layer| {
                let name = unsafe { CStr::from_ptr(layer.layer_name.as_ptr()) };
                let name = name.to_str().expect("Failed to get layer name pointer");
                required == &name
            });

        if !found {
            panic!("Validation layer not supported: {}", required);
        }
    }
}

#[derive(Clone, Debug)]
pub struct SwapchainSupport {
    pub capabilities: vk::SurfaceCapabilitiesKHR,
    pub formats: Vec<vk::SurfaceFormatKHR>,
    pub present_modes: Vec<vk::PresentModeKHR>,
}

impl SwapchainSupport {
    pub unsafe fn get(
        surface_extension: &ash::khr::surface::Instance,
        surface: vk::SurfaceKHR,
        physical_device: vk::PhysicalDevice,
    ) -> Result<Self> {
        let capabilities =
            surface_extension.get_physical_device_surface_capabilities(physical_device, surface)?;
        let formats =
            surface_extension.get_physical_device_surface_formats(physical_device, surface)?;
        let present_modes = surface_extension
            .get_physical_device_surface_present_modes(physical_device, surface)?;

        Ok(Self {
            capabilities,
            formats,
            present_modes,
        })
    }
}

#[derive(Copy, Clone, Debug)]
pub struct QueueFamilyIndices {
    pub graphics: u32,
    // present: u32,
}

impl QueueFamilyIndices {
    pub(crate) unsafe fn get(
        instance: &ash::Instance,
        physical_device: vk::PhysicalDevice,
    ) -> Result<Self> {
        let properties = instance.get_physical_device_queue_family_properties(physical_device);
        let graphics = properties
            .iter()
            .position(|p| p.queue_flags.contains(vk::QueueFlags::GRAPHICS))
            .map(|i| i as u32);

        if let Some(graphics) = graphics {
            Ok(Self { graphics })
        } else {
            Err(anyhow!(SuitabilityError(
                "Missing required queue families."
            )))
        }
    }
}

pub unsafe fn pick_physical_device(
    instance: &ash::Instance,
    surface_extension: &ash::khr::surface::Instance,
    surface: vk::SurfaceKHR,
) -> Result<vk::PhysicalDevice> {
    for physical_device in instance.enumerate_physical_devices()? {
        let properties = instance.get_physical_device_properties(physical_device);

        let device_name = CStr::from_ptr(properties.device_name.as_ptr())
            .to_str()
            .unwrap();
        if let Err(error) = check_physical_device(
            instance,
            surface_extension,
            surface,
            physical_device,
            properties,
        ) {
            warn!("Skipping physical device {}: {}", device_name, error);
        } else {
            info!("Selected physical device {}.", device_name);
            return Ok(physical_device);
        }
    }

    Err(anyhow!("Failed to find a suitable physical device"))
}

unsafe fn check_physical_device(
    instance: &ash::Instance,
    surface_extension: &ash::khr::surface::Instance,
    surface: vk::SurfaceKHR,
    physical_device: vk::PhysicalDevice,
    properties: vk::PhysicalDeviceProperties,
) -> Result<()> {
    QueueFamilyIndices::get(instance, physical_device)?;
    check_physical_device_extensions(instance, physical_device)?;

    if properties.api_version < vk::make_api_version(0, 1, 3, 0) {
        return Err(anyhow!(SuitabilityError(
            "Failed to find a physical device compatible with vulkan 1.3"
        )));
    }

    check_vulkan_features(instance, physical_device)?;

    let support = SwapchainSupport::get(surface_extension, surface, physical_device)?;
    if support.formats.is_empty() || support.present_modes.is_empty() {
        return Err(anyhow!(SuitabilityError("Insufficient swapchain support.")));
    }

    Ok(())
}

unsafe fn check_vulkan_features(
    instance: &ash::Instance,
    physical_device: vk::PhysicalDevice,
) -> Result<()> {
    let mut vulkan_12_features = vk::PhysicalDeviceVulkan12Features::default();
    let mut features_2 = vk::PhysicalDeviceFeatures2::default().push_next(&mut vulkan_12_features);

    instance.get_physical_device_features2(physical_device, &mut features_2);

    // If you want more vulkan 1.2 or 1.3 features, just add the checks here
    if vulkan_12_features.descriptor_indexing == vk::FALSE {
        return Err(anyhow!(SuitabilityError(
            "Physical Device doesn't support Vulkan 1.2 descriptor indexing"
        )));
    }

    if vulkan_12_features.buffer_device_address == vk::FALSE {
        return Err(anyhow!(SuitabilityError(
            "Physical Device doesn't support Vulkan 1.2 buffer device address"
        )));
    }

    let mut vulkan_13_features = vk::PhysicalDeviceVulkan13Features::default();
    let mut features_2 = vk::PhysicalDeviceFeatures2::default().push_next(&mut vulkan_13_features);

    instance.get_physical_device_features2(physical_device, &mut features_2);

    if vulkan_13_features.dynamic_rendering == vk::FALSE {
        return Err(anyhow!(SuitabilityError(
            "Physical Device doesn't support Vulkan 1.3 dynamic rendering"
        )));
    }

    if vulkan_13_features.synchronization2 == vk::FALSE {
        return Err(anyhow!(SuitabilityError(
            "Physical Device doesn't support Vulkan 1.3 synchronization 2"
        )));
    }

    Ok(())
}

unsafe fn check_physical_device_extensions(
    instance: &ash::Instance,
    physical_device: vk::PhysicalDevice,
) -> Result<()> {
    let extensions = instance
        .enumerate_device_extension_properties(physical_device)?
        .iter()
        .map(|e| CStr::from_ptr(e.extension_name.as_ptr()))
        .collect::<HashSet<_>>();

    // TODO: Maybe put this in constant ?
    let device_extension_names_raw = [ash::khr::swapchain::NAME.as_ptr()];

    if device_extension_names_raw
        .iter()
        .all(|e| extensions.contains(CStr::from_ptr(*e)))
    {
        Ok(())
    } else {
        Err(anyhow!(SuitabilityError(
            "Missing required device extensions."
        )))
    }
}

pub struct LogicalDeviceCreationData {
    pub device: ash::Device,
    pub graphics_queue: vk::Queue,
}

pub unsafe fn create_logical_device(
    instance: &ash::Instance,
    physical_device: vk::PhysicalDevice,
) -> Result<LogicalDeviceCreationData> {
    let queue_family_indices = QueueFamilyIndices::get(instance, physical_device)?;

    let queue_priorities = &[1.0];
    let queue_infos = {
        let mut indices = vec![queue_family_indices.graphics];
        indices.dedup();
        indices
            .iter()
            .map(|i| {
                vk::DeviceQueueCreateInfo::default()
                    .queue_family_index(*i)
                    .queue_priorities(queue_priorities)
            })
            .collect::<Vec<_>>()
    };

    let extensions = vec![ash::khr::swapchain::NAME.as_ptr()];

    let features = vk::PhysicalDeviceFeatures::default();
    let mut dynamic_rendering =
        vk::PhysicalDeviceDynamicRenderingFeatures::default().dynamic_rendering(true);
    let mut sync2 = vk::PhysicalDeviceSynchronization2Features::default().synchronization2(true);
    let mut buffer_device_address =
        vk::PhysicalDeviceBufferDeviceAddressFeatures::default().buffer_device_address(true);
    let info = vk::DeviceCreateInfo::default()
        .queue_create_infos(queue_infos.as_slice())
        .enabled_extension_names(&extensions)
        .enabled_features(&features)
        .push_next(&mut dynamic_rendering)
        .push_next(&mut sync2)
        .push_next(&mut buffer_device_address);

    let device = instance.create_device(physical_device, &info, None)?;
    let graphics_queue = device.get_device_queue(queue_family_indices.graphics, 0);

    Ok(LogicalDeviceCreationData {
        device,
        graphics_queue,
    })
}

pub extern "system" fn debug_callback(
    severity: vk::DebugUtilsMessageSeverityFlagsEXT,
    type_: vk::DebugUtilsMessageTypeFlagsEXT,
    data: *const vk::DebugUtilsMessengerCallbackDataEXT,
    _: *mut c_void,
) -> vk::Bool32 {
    let data = unsafe { *data };
    let message = unsafe { CStr::from_ptr(data.p_message) };

    match severity {
        vk::DebugUtilsMessageSeverityFlagsEXT::VERBOSE => debug!("({type_:?}) {message:?}"),
        vk::DebugUtilsMessageSeverityFlagsEXT::INFO => info!("({type_:?}) {message:?}"),
        vk::DebugUtilsMessageSeverityFlagsEXT::WARNING => warn!("({type_:?}) {message:?}"),
        _ => error!("({type_:?}) {message:?}"),
    }

    vk::FALSE
}

pub unsafe fn transition_image_layout(
    device: &ash::Device,
    command: vk::CommandBuffer,
    image: vk::Image,
    current_layout: vk::ImageLayout,
    new_layout: vk::ImageLayout,
) -> Result<()> {
    let aspect_mask = if new_layout == vk::ImageLayout::DEPTH_ATTACHMENT_OPTIMAL {
        vk::ImageAspectFlags::DEPTH
    } else {
        vk::ImageAspectFlags::COLOR
    };

    let image_subresource_range = vk::ImageSubresourceRange::default()
        .aspect_mask(aspect_mask)
        .level_count(vk::REMAINING_MIP_LEVELS)
        .layer_count(vk::REMAINING_ARRAY_LAYERS);

    // TODO: Tweak transition functions
    // Un mois plus tard : mon frère en christe, je dois tweak quoi ?
    let image_barrier = vk::ImageMemoryBarrier2::default()
        .src_stage_mask(vk::PipelineStageFlags2::ALL_COMMANDS)
        .src_access_mask(vk::AccessFlags2::MEMORY_WRITE)
        .dst_stage_mask(vk::PipelineStageFlags2::ALL_COMMANDS)
        .dst_access_mask(vk::AccessFlags2::MEMORY_WRITE | vk::AccessFlags2::MEMORY_READ)
        .old_layout(current_layout)
        .new_layout(new_layout)
        .subresource_range(image_subresource_range)
        .image(image);

    let image_barriers = &[image_barrier];
    let dependency_info = vk::DependencyInfo::default().image_memory_barriers(image_barriers);
    device.cmd_pipeline_barrier2(command, &dependency_info);

    Ok(())
}

/// Get the pointers to the validation layers names.
/// Also return the corresponding `CString` to avoid dangling pointers.
pub fn get_layer_names_and_pointers() -> (Vec<CString>, Vec<*const c_char>) {
    let layer_names = REQUIRED_LAYERS
        .iter()
        .map(|name| CString::new(*name).unwrap())
        .collect::<Vec<_>>();
    let layer_names_ptrs = layer_names
        .iter()
        .map(|name| name.as_ptr())
        .collect::<Vec<_>>();
    (layer_names, layer_names_ptrs)
}

pub fn copy_image_to_image(
    device: &ash::Device,
    command: vk::CommandBuffer,
    source: vk::Image,
    destination: vk::Image,
    source_size: vk::Extent2D,
    destination_size: vk::Extent2D,
) {
    let source_subresource = vk::ImageSubresourceLayers::default()
        .aspect_mask(vk::ImageAspectFlags::COLOR)
        .base_array_layer(0)
        .layer_count(1)
        .mip_level(0);
    let destination_subresource = vk::ImageSubresourceLayers::default()
        .aspect_mask(vk::ImageAspectFlags::COLOR)
        .base_array_layer(0)
        .layer_count(1)
        .mip_level(0);

    let blit_region = vk::ImageBlit2::default()
        .src_offsets([
            Default::default(),
            Offset3D {
                x: source_size.width as i32,
                y: source_size.height as i32,
                z: 1,
            },
        ])
        .dst_offsets([
            Default::default(),
            Offset3D {
                x: destination_size.width as i32,
                y: destination_size.height as i32,
                z: 1,
            },
        ])
        .src_subresource(source_subresource)
        .dst_subresource(destination_subresource);

    let regions = &[blit_region];
    let blit_info = vk::BlitImageInfo2::default()
        .dst_image(destination)
        .dst_image_layout(vk::ImageLayout::TRANSFER_DST_OPTIMAL)
        .src_image(source)
        .src_image_layout(vk::ImageLayout::TRANSFER_SRC_OPTIMAL)
        .filter(vk::Filter::LINEAR)
        .regions(regions);

    unsafe { device.cmd_blit_image2(command, &blit_info) };
}
