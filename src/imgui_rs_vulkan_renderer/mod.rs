// Taken from : https://github.com/adrien-ben/imgui-rs-vulkan-renderer/tree/master
// Because I wanted the last version of Ash

// MIT License
//
// Copyright (c) 2020 Adrien Bennadji
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
// associated documentation files (the "Software"), to deal in the Software without restriction,
// including without limitation the rights to use, copy, modify, merge, publish, distribute,
// sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
// NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

mod renderer;

use ash::vk;
use imgui::TextureId;
use thiserror::Error;

#[derive(Debug, Error)]
pub enum RendererError {
    /// Errors coming from calls to Vulkan functions.
    #[error("A Vulkan error occured: {0}")]
    Vulkan(#[from] vk::Result),

    /// Io errors.
    #[error("A io error occured: {0}")]
    Io(#[from] std::io::Error),

    /// Initialization errors.
    #[error("An error occured when initializing the renderer: {0}")]
    Init(String),

    /// Texture lookup error.
    #[error("Bad texture ID: {}", .0.id())]
    BadTexture(TextureId),

    /// Allocator error
    #[error("A error occured when using the allocator: {0}")]
    Allocator(String),
}

pub use renderer::*;
