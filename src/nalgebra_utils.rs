pub fn array_to_vec4(array: [f32; 4]) -> glm::Vec4 {
    glm::Vec4::new(array[0], array[1], array[2], array[3])
}

pub fn array_to_vec3(array: [f32; 3]) -> glm::Vec3 {
    glm::Vec3::new(array[0], array[1], array[2])
}

pub fn array_to_point3(array: [f32; 3]) -> na::Point3<f32> {
    na::Point3::new(array[0], array[1], array[2])
}

pub fn vec4_to_array(vec: glm::Vec4) -> [f32; 4] {
    [vec.x, vec.y, vec.z, vec.w]
}
