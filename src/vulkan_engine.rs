#![allow(deprecated)]

use crate::imgui_rs_vulkan_renderer;
use crate::imgui_rs_vulkan_renderer::Renderer as ImguiRenderer;
use crate::nalgebra_utils::*;
use crate::vulkan_descriptors::*;
use crate::vulkan_pipelines::*;
use crate::vulkan_types::*;
use crate::vulkan_utils::*;
use anyhow::{anyhow, Result};
use ash::vk;
use imgui::{Condition, FontConfig, FontSource};
use imgui_winit_support::{HiDpiMode, WinitPlatform};
use raw_window_handle::{HasRawDisplayHandle, HasRawWindowHandle};
use std::mem::ManuallyDrop;
use std::sync::{Arc, Mutex};
use std::time::{Duration, Instant};
use vk_mem::Alloc;
use winit::application::ApplicationHandler;
use winit::dpi::{LogicalSize, PhysicalSize};
use winit::event::WindowEvent;
use winit::event_loop::ActiveEventLoop;
use winit::window::{Window, WindowAttributes, WindowId};

pub const VALIDATION_ENABLED: bool = cfg!(debug_assertions);
const FRAME_OVERLAP: usize = 2;
const ONE_SECOND_IN_NANO_SEC: u64 = 1000000000;

#[derive(Default)]
pub struct VulkanEngine {
    graphic_data: Option<GraphicData>,
}

pub struct ImguiData {
    pub imgui_ctx: imgui::Context,
    pub imgui_platform: WinitPlatform,
    pub imgui_renderer: imgui_rs_vulkan_renderer::Renderer,
}

/// This is data that needs winit's window to load
/// It's separated because winit changed its api AGAIN and I need to create VulkanEngine before getting the window
struct GraphicData {
    frame_number: usize,
    should_stop_rendering: bool,
    minimized: bool,
    resize_requested: bool,
    render_scale: f32,
    current_background_effect: usize,
    last_time: Instant,
    window: Window,
    /// This is the "real" window size given by winit
    window_size: PhysicalSize<u32>,
    /// This is the size that vulkan takes into account
    window_extent: vk::Extent2D,
    entry: ash::Entry,
    instance: ash::Instance,
    debug_extension: ash::ext::debug_utils::Instance,
    debug_messenger: vk::DebugUtilsMessengerEXT,
    physical_device: vk::PhysicalDevice,
    device: ash::Device,
    surface_extension: ash::khr::surface::Instance,
    surface: vk::SurfaceKHR,
    swapchain_extension: ash::khr::swapchain::Device,
    swapchain: vk::SwapchainKHR,
    swapchain_image_format: vk::Format,
    swapchain_images: Vec<vk::Image>,
    swapchain_image_views: Vec<vk::ImageView>,
    swapchain_extent: vk::Extent2D,
    graphics_queue: vk::Queue,
    frames: [FrameData; FRAME_OVERLAP],
    gpu_allocator: ManuallyDrop<Arc<Mutex<vk_mem::Allocator>>>,
    draw_image: AllocatedImage,
    depth_image: AllocatedImage,
    draw_extent: vk::Extent2D,
    global_descriptor_allocator: DescriptorAllocator,
    draw_image_descriptors: vk::DescriptorSet,
    draw_image_descriptor_layout: vk::DescriptorSetLayout,
    gradient_pipeline_layout: vk::PipelineLayout,
    immediate_fence: vk::Fence,
    immediate_command_buffer: vk::CommandBuffer,
    immediate_command_pool: vk::CommandPool,
    imgui_data: Option<ImguiData>,
    background_effects: Vec<ComputeEffect>,
    single_image_descriptor_layout: vk::DescriptorSetLayout,
}

impl GraphicData {
    fn load(event_loop: &ActiveEventLoop) -> Result<GraphicData> {
        const WIDTH: u32 = 1024;
        const HEIGHT: u32 = 768;

        let window_attributes = WindowAttributes::default()
            .with_title("Prout V2")
            .with_inner_size(LogicalSize::new(WIDTH, HEIGHT));

        let window = event_loop.create_window(window_attributes).unwrap();

        let window_extent = vk::Extent2D {
            width: window.inner_size().width,
            height: window.inner_size().height,
        };

        let mut imgui = imgui::Context::create();
        imgui.io_mut().config_flags |= imgui::ConfigFlags::DOCKING_ENABLE;
        imgui.io_mut().config_flags |= imgui::ConfigFlags::VIEWPORTS_ENABLE;
        let mut platform = WinitPlatform::init(&mut imgui);

        platform.attach_window(imgui.io_mut(), &window, HiDpiMode::Rounded);
        let hidpi_factor = platform.hidpi_factor();
        let font_size = (13.0 * hidpi_factor) as f32;
        imgui.fonts().add_font(&[FontSource::DefaultFontData {
            config: Some(FontConfig {
                size_pixels: font_size,
                ..Default::default()
            }),
        }]);
        imgui.io_mut().font_global_scale = (1.0 / hidpi_factor) as f32;

        let vulkan_init_data = unsafe { Self::init_vulkan(&window)? };
        let surface_extension =
            ash::khr::surface::Instance::new(&vulkan_init_data.entry, &vulkan_init_data.instance);

        let surface = unsafe {
            ash_window::create_surface(
                &vulkan_init_data.entry,
                &vulkan_init_data.instance,
                window.raw_display_handle()?,
                window.raw_window_handle()?,
                None,
            )?
        };
        let physical_device = unsafe {
            pick_physical_device(&vulkan_init_data.instance, &surface_extension, surface)?
        };
        let logical_device_creation_data =
            unsafe { create_logical_device(&vulkan_init_data.instance, physical_device)? };

        let mut alloc_info = vk_mem::AllocatorCreateInfo::new(
            &vulkan_init_data.instance,
            &logical_device_creation_data.device,
            physical_device,
        );
        alloc_info.flags = vk_mem::AllocatorCreateFlags::BUFFER_DEVICE_ADDRESS;
        alloc_info.vulkan_api_version = vk::API_VERSION_1_3;

        let gpu_allocator = ManuallyDrop::new(Arc::new(Mutex::new(unsafe {
            vk_mem::Allocator::new(alloc_info)
        }?)));

        let swapchain_creation_data = unsafe {
            Self::init_swapchain(&CreateSwapchainInfo {
                window: &window,
                instance: &vulkan_init_data.instance,
                device: &logical_device_creation_data.device,
                surface_extension: &surface_extension,
                surface,
                physical_device,
                gpu_allocator: ManuallyDrop::into_inner(gpu_allocator.clone()),
            })?
        };

        let (mut frames, immediate_command_pool, immediate_command_buffer) = unsafe {
            Self::init_commands(
                &vulkan_init_data.instance,
                &logical_device_creation_data.device,
                physical_device,
            )?
        };

        let immediate_fence = unsafe {
            Self::init_sync_structures(&mut frames, &logical_device_creation_data.device)?
        };

        let imgui_renderer = ImguiRenderer::with_vk_mem_allocator(
            ManuallyDrop::into_inner(gpu_allocator.clone()),
            logical_device_creation_data.device.clone(),
            logical_device_creation_data.graphics_queue,
            immediate_command_pool,
            crate::imgui_rs_vulkan_renderer::DynamicRendering {
                color_attachment_format: swapchain_creation_data.swapchain_format,
                depth_attachment_format: None,
            },
            &mut imgui,
            Some(crate::imgui_rs_vulkan_renderer::Options {
                in_flight_frames: FRAME_OVERLAP,
                ..Default::default()
            }),
        )?;

        let descriptors_init_data = unsafe {
            Self::init_descriptors(
                &logical_device_creation_data.device,
                &swapchain_creation_data.draw_image,
                &mut frames,
            )?
        };

        let (compute_effects, gradient_pipeline_layout) = unsafe {
            Self::init_pipelines(
                &logical_device_creation_data.device,
                descriptors_init_data.draw_image_descriptor_layout,
            )?
        };

        Ok(Self {
            render_scale: 1.0,
            frame_number: 0,
            current_background_effect: 0,
            last_time: Instant::now(),
            minimized: false,
            resize_requested: false,
            should_stop_rendering: false,
            window,
            window_extent,
            window_size: PhysicalSize {
                height: window_extent.height,
                width: window_extent.width,
            },
            entry: vulkan_init_data.entry,
            instance: vulkan_init_data.instance,
            debug_extension: vulkan_init_data.debug_extension,
            debug_messenger: vulkan_init_data.debug_messenger,
            surface_extension,
            surface,
            physical_device,
            device: logical_device_creation_data.device,
            graphics_queue: logical_device_creation_data.graphics_queue,
            swapchain_extension: swapchain_creation_data.swapchain_extension,
            swapchain: swapchain_creation_data.swapchain,
            swapchain_extent: swapchain_creation_data.swapchain_extent,
            swapchain_images: swapchain_creation_data.swapchain_images,
            swapchain_image_format: swapchain_creation_data.swapchain_format,
            swapchain_image_views: swapchain_creation_data.swapchain_image_views,
            frames,
            gpu_allocator,
            draw_image: swapchain_creation_data.draw_image,
            draw_extent: window_extent,
            global_descriptor_allocator: descriptors_init_data.global_descriptor_allocator,
            draw_image_descriptors: descriptors_init_data.draw_image_descriptors,
            draw_image_descriptor_layout: descriptors_init_data.draw_image_descriptor_layout,
            gradient_pipeline_layout,
            immediate_command_buffer,
            immediate_command_pool,
            immediate_fence,
            imgui_data: Some(ImguiData {
                imgui_ctx: imgui,
                imgui_platform: platform,
                imgui_renderer,
            }),
            background_effects: compute_effects,
            depth_image: swapchain_creation_data.depth_image,
            single_image_descriptor_layout: descriptors_init_data.single_image_descriptor_layout,
        })
    }

    unsafe fn init_vulkan(window: &Window) -> Result<VulkanInitializationData> {
        let entry = ash::Entry::linked();
        let instance = create_instance(window, &entry)?;

        let debug_extension = ash::ext::debug_utils::Instance::new(&entry, &instance);

        let mut debug_messenger: vk::DebugUtilsMessengerEXT = Default::default();
        if VALIDATION_ENABLED {
            let debug_info = vk::DebugUtilsMessengerCreateInfoEXT::default()
                .message_severity(
                    vk::DebugUtilsMessageSeverityFlagsEXT::VERBOSE
                        | vk::DebugUtilsMessageSeverityFlagsEXT::INFO
                        | vk::DebugUtilsMessageSeverityFlagsEXT::WARNING
                        | vk::DebugUtilsMessageSeverityFlagsEXT::ERROR,
                )
                .message_type(
                    vk::DebugUtilsMessageTypeFlagsEXT::GENERAL
                        | vk::DebugUtilsMessageTypeFlagsEXT::DEVICE_ADDRESS_BINDING
                        | vk::DebugUtilsMessageTypeFlagsEXT::PERFORMANCE
                        | vk::DebugUtilsMessageTypeFlagsEXT::VALIDATION,
                )
                .pfn_user_callback(Some(debug_callback));

            debug_messenger = debug_extension.create_debug_utils_messenger(&debug_info, None)?;
        }

        Ok(VulkanInitializationData {
            entry,
            instance,
            debug_extension,
            debug_messenger,
        })
    }

    unsafe fn init_commands(
        instance: &ash::Instance,
        device: &ash::Device,
        physical_device: vk::PhysicalDevice,
    ) -> Result<(
        [FrameData; FRAME_OVERLAP],
        vk::CommandPool,
        vk::CommandBuffer,
    )> {
        let mut frames = core::array::from_fn(|_| FrameData::default());
        for frame in &mut frames {
            frame.command_pool = create_command_pool(instance, device, physical_device)?;
            let allocate_info = vk::CommandBufferAllocateInfo::default()
                .command_pool(frame.command_pool)
                .level(vk::CommandBufferLevel::PRIMARY)
                .command_buffer_count(1);

            frame.command_buffer = device.allocate_command_buffers(&allocate_info)?[0];
        }

        let immediate_command_pool = create_command_pool(instance, device, physical_device)?;
        let command_alloc_info = vk::CommandBufferAllocateInfo::default()
            .command_pool(immediate_command_pool)
            .level(vk::CommandBufferLevel::PRIMARY)
            .command_buffer_count(1);

        let immediate_command_buffer = device.allocate_command_buffers(&command_alloc_info)?[0];

        Ok((frames, immediate_command_pool, immediate_command_buffer))
    }

    unsafe fn init_sync_structures(
        frames: &mut [FrameData; FRAME_OVERLAP],
        device: &ash::Device,
    ) -> Result<vk::Fence> {
        let semaphore_info = vk::SemaphoreCreateInfo::default();
        let fence_info = vk::FenceCreateInfo::default().flags(vk::FenceCreateFlags::SIGNALED);

        for frames in frames {
            frames.render_fence = device.create_fence(&fence_info, None)?;
            frames.swapchain_semaphore = device.create_semaphore(&semaphore_info, None)?;
            frames.render_semaphore = device.create_semaphore(&semaphore_info, None)?;
        }

        let immediate_fence = device.create_fence(&fence_info, None)?;

        Ok(immediate_fence)
    }

    unsafe fn init_descriptors(
        device: &ash::Device,
        draw_image: &AllocatedImage,
        frames: &mut [FrameData; FRAME_OVERLAP],
    ) -> Result<DescriptorsInitializationData> {
        let sizes = [PoolSizeRatio::new(vk::DescriptorType::STORAGE_IMAGE, 1.0)];

        let global_descriptor_allocator = DescriptorAllocator::new(device, 10, &sizes)?;

        let mut builder = DescriptorLayoutBuilder::new();
        builder.add_binding(0, vk::DescriptorType::STORAGE_IMAGE);
        let draw_image_descriptor_layout = builder.build(device, vk::ShaderStageFlags::COMPUTE)?;

        let draw_image_descriptors =
            global_descriptor_allocator.allocate(device, draw_image_descriptor_layout)?;

        let image_info = vk::DescriptorImageInfo::default()
            .image_layout(vk::ImageLayout::GENERAL)
            .image_view(draw_image.image_view);

        let image_infos = &[image_info];
        let draw_image_write = vk::WriteDescriptorSet::default()
            .dst_binding(0)
            .dst_set(draw_image_descriptors)
            .descriptor_type(vk::DescriptorType::STORAGE_IMAGE)
            .image_info(image_infos);

        let descriptor_write = &[draw_image_write];
        device.update_descriptor_sets(descriptor_write, &[]);

        for frame in frames {
            let frame_sizes = [
                PoolSizeRatio::new(vk::DescriptorType::STORAGE_IMAGE, 3.0),
                PoolSizeRatio::new(vk::DescriptorType::STORAGE_BUFFER, 3.0),
                PoolSizeRatio::new(vk::DescriptorType::UNIFORM_BUFFER, 3.0),
                PoolSizeRatio::new(vk::DescriptorType::COMBINED_IMAGE_SAMPLER, 4.0),
            ];

            frame.frame_descriptors = DescriptorAllocatorGrowable::new(device, 1000, &frame_sizes)?;
        }

        let single_image_descriptor_layout = {
            let mut builder = DescriptorLayoutBuilder::new();
            builder.add_binding(0, vk::DescriptorType::COMBINED_IMAGE_SAMPLER);
            builder.build(device, vk::ShaderStageFlags::FRAGMENT)?
        };

        Ok(DescriptorsInitializationData {
            global_descriptor_allocator,
            draw_image_descriptors,
            draw_image_descriptor_layout,
            single_image_descriptor_layout,
        })
    }

    unsafe fn init_pipelines(
        device: &ash::Device,
        draw_image_descriptor_layout: vk::DescriptorSetLayout,
    ) -> Result<(Vec<ComputeEffect>, vk::PipelineLayout)> {
        let background_data =
            Self::init_background_pipelines(device, draw_image_descriptor_layout)?;

        Ok((background_data.0, background_data.1))
    }

    unsafe fn init_background_pipelines(
        device: &ash::Device,
        draw_image_descriptor_layout: vk::DescriptorSetLayout,
    ) -> Result<(Vec<ComputeEffect>, vk::PipelineLayout)> {
        let push_constant = vk::PushConstantRange::default()
            .offset(0)
            .size(std::mem::size_of::<ComputePushConstants>() as u32)
            .stage_flags(vk::ShaderStageFlags::COMPUTE);

        let set_layouts = &[draw_image_descriptor_layout];
        let push_constants = &[push_constant];
        let compute_layout = vk::PipelineLayoutCreateInfo::default()
            .set_layouts(set_layouts)
            .push_constant_ranges(push_constants);

        let gradient_pipeline_layout = device.create_pipeline_layout(&compute_layout, None)?;

        let gradient_shader = load_shader_module_from_bytes(
            include_bytes!("shaders/gradient_color.comp.spv"),
            device,
        )?;
        let sky_shader =
            load_shader_module_from_bytes(include_bytes!("shaders/sky.comp.spv"), device)?;

        let stage_info = vk::PipelineShaderStageCreateInfo::default()
            .stage(vk::ShaderStageFlags::COMPUTE)
            .module(gradient_shader)
            .name(c"main");

        let mut compute_pipeline_create_info = vk::ComputePipelineCreateInfo::default()
            .layout(gradient_pipeline_layout)
            .stage(stage_info);

        let create_infos = &[compute_pipeline_create_info];
        let gradient_pipeline = device
            .create_compute_pipelines(vk::PipelineCache::null(), create_infos, None)
            .map_err(|e| e.1)?[0];

        let gradient_effect = ComputeEffect {
            layout: gradient_pipeline_layout,
            name: String::from("gradient"),
            data: ComputePushConstants {
                data1: glm::Vec4::new(1.0, 0.0, 0.0, 1.0),
                data2: glm::Vec4::new(0.0, 0.0, 1.0, 1.0),
                ..Default::default()
            },
            pipeline: gradient_pipeline,
        };

        compute_pipeline_create_info.stage.module = sky_shader;

        let create_infos = &[compute_pipeline_create_info];
        let sky_pipeline = device
            .create_compute_pipelines(vk::PipelineCache::null(), create_infos, None)
            .map_err(|e| e.1)?[0];

        let sky_effect = ComputeEffect {
            layout: gradient_pipeline_layout,
            name: String::from("sky"),
            data: ComputePushConstants {
                data1: glm::Vec4::new(0.1, 0.2, 0.4, 0.97),
                ..Default::default()
            },
            pipeline: sky_pipeline,
        };

        device.destroy_shader_module(gradient_shader, None);
        device.destroy_shader_module(sky_shader, None);

        Ok((vec![gradient_effect, sky_effect], gradient_pipeline_layout))
    }

    unsafe fn create_swapchain(
        instance: &ash::Instance,
        device: &ash::Device,
        window: &Window,
        physical_device: vk::PhysicalDevice,
        surface_extension: &ash::khr::surface::Instance,
        surface: vk::SurfaceKHR,
        extent: Option<vk::Extent2D>,
    ) -> Result<SwapchainCreationData> {
        let indices = QueueFamilyIndices::get(instance, physical_device)?;
        let support = SwapchainSupport::get(surface_extension, surface, physical_device)?;

        let surface_format = get_swapchain_surface_format(&support.formats);
        let present_mode = get_swapchain_present_mode(&support.present_modes);
        let extent = extent.unwrap_or(get_swapchain_extent(window, support.capabilities));
        let mut image_count = support.capabilities.min_image_count + 1;
        if support.capabilities.max_image_count != 0
            && image_count > support.capabilities.max_image_count
        {
            image_count = support.capabilities.max_image_count;
        }

        let queue_family_indices = &[indices.graphics];

        let info = vk::SwapchainCreateInfoKHR::default()
            .surface(surface)
            .min_image_count(image_count)
            .image_format(surface_format.format)
            .image_color_space(surface_format.color_space)
            .image_extent(extent)
            .image_array_layers(1)
            .image_usage(vk::ImageUsageFlags::COLOR_ATTACHMENT | vk::ImageUsageFlags::TRANSFER_DST)
            .image_sharing_mode(vk::SharingMode::EXCLUSIVE)
            .queue_family_indices(queue_family_indices)
            .pre_transform(support.capabilities.current_transform)
            .composite_alpha(vk::CompositeAlphaFlagsKHR::OPAQUE)
            .present_mode(present_mode)
            .clipped(true)
            .old_swapchain(vk::SwapchainKHR::null());

        let swapchain_extension = ash::khr::swapchain::Device::new(&instance, &device);
        let swapchain = swapchain_extension.create_swapchain(&info, None)?;
        let swapchain_images = swapchain_extension.get_swapchain_images(swapchain)?;
        let swapchain_format = surface_format.format;
        let swapchain_extent = extent;
        let swapchain_image_views =
            create_swapchain_image_views(device, &swapchain_images, swapchain_format)?;

        Ok(SwapchainCreationData {
            swapchain_format,
            swapchain_extent,
            swapchain,
            swapchain_image_views,
            swapchain_images,
            swapchain_extension,
        })
    }

    pub unsafe fn init_swapchain(
        create_info: &CreateSwapchainInfo,
    ) -> Result<SwapchainInitCreationData> {
        let screen_size = create_info.window.current_monitor().unwrap().size();

        let create_data = Self::create_swapchain(
            create_info.instance,
            create_info.device,
            create_info.window,
            create_info.physical_device,
            create_info.surface_extension,
            create_info.surface,
            None,
        )?;

        let draw_image_extent = vk::Extent3D {
            width: screen_size.width,
            height: screen_size.height,
            depth: 1,
        };

        let draw_image_format = vk::Format::R16G16B16A16_SFLOAT;

        let draw_image_usages = vk::ImageUsageFlags::TRANSFER_SRC
            | vk::ImageUsageFlags::TRANSFER_DST
            | vk::ImageUsageFlags::STORAGE
            | vk::ImageUsageFlags::COLOR_ATTACHMENT;

        let draw_image_info = vk::ImageCreateInfo::default()
            .image_type(vk::ImageType::TYPE_2D)
            .format(draw_image_format)
            .extent(draw_image_extent)
            .mip_levels(1)
            .array_layers(1)
            .samples(vk::SampleCountFlags::TYPE_1)
            .tiling(vk::ImageTiling::OPTIMAL)
            .usage(draw_image_usages);

        let render_image_alloc_info = vk_mem::AllocationCreateInfo {
            usage: vk_mem::MemoryUsage::AutoPreferDevice,
            flags: vk_mem::AllocationCreateFlags::DEDICATED_MEMORY,
            required_flags: vk::MemoryPropertyFlags::DEVICE_LOCAL,
            ..Default::default()
        };

        let (render_image, render_image_allocation) = create_info
            .gpu_allocator
            .lock()
            .unwrap()
            .create_image(&draw_image_info, &render_image_alloc_info)?;

        let subresource_range = vk::ImageSubresourceRange::default()
            .base_mip_level(0)
            .level_count(1)
            .base_array_layer(0)
            .layer_count(1)
            .aspect_mask(vk::ImageAspectFlags::COLOR);
        let render_view_info = vk::ImageViewCreateInfo::default()
            .view_type(vk::ImageViewType::TYPE_2D)
            .image(render_image)
            .format(draw_image_format)
            .subresource_range(subresource_range);
        let image_view = create_info
            .device
            .create_image_view(&render_view_info, None)?;

        let draw_image = AllocatedImage {
            image: render_image,
            image_view,
            image_extent: draw_image_extent,
            image_format: draw_image_format,
            gpu_allocation: render_image_allocation,
        };

        let depth_image_info = vk::ImageCreateInfo::default()
            .image_type(vk::ImageType::TYPE_2D)
            .format(vk::Format::D16_UNORM)
            .extent(draw_image_extent)
            .mip_levels(1)
            .array_layers(1)
            .samples(vk::SampleCountFlags::TYPE_1)
            .tiling(vk::ImageTiling::OPTIMAL)
            .usage(vk::ImageUsageFlags::DEPTH_STENCIL_ATTACHMENT);

        let (depth_image, depth_image_allocation) = create_info
            .gpu_allocator
            .lock()
            .unwrap()
            .create_image(&depth_image_info, &render_image_alloc_info)?;

        let subresource_range = vk::ImageSubresourceRange::default()
            .base_mip_level(0)
            .level_count(1)
            .base_array_layer(0)
            .layer_count(1)
            .aspect_mask(vk::ImageAspectFlags::DEPTH);
        let depth_view_info = vk::ImageViewCreateInfo::default()
            .view_type(vk::ImageViewType::TYPE_2D)
            .image(depth_image)
            .format(depth_image_info.format)
            .subresource_range(subresource_range);
        let depth_image_view = create_info
            .device
            .create_image_view(&depth_view_info, None)?;

        let depth_image = AllocatedImage {
            image: depth_image,
            image_view: depth_image_view,
            image_extent: draw_image_extent,
            image_format: depth_image_info.format,
            gpu_allocation: depth_image_allocation,
        };

        Ok(SwapchainInitCreationData {
            swapchain_extension: create_data.swapchain_extension,
            swapchain: create_data.swapchain,
            swapchain_images: create_data.swapchain_images,
            swapchain_format: create_data.swapchain_format,
            swapchain_extent: create_data.swapchain_extent,
            draw_image,
            depth_image,
            swapchain_image_views: create_data.swapchain_image_views,
        })
    }

    unsafe fn destroy_swapchain(&mut self) {
        self.swapchain_image_views
            .iter()
            .for_each(|v| self.device.destroy_image_view(*v, None));
        self.swapchain_extension
            .destroy_swapchain(self.swapchain, None);
    }

    unsafe fn draw_imgui(
        &mut self,
        command: vk::CommandBuffer,
        target_image_view: vk::ImageView,
        imgui_data: &mut ImguiData,
    ) -> Result<()> {
        let color_attachment = [vk::RenderingAttachmentInfo::default()
            .image_view(target_image_view)
            .image_layout(vk::ImageLayout::GENERAL)
            .load_op(vk::AttachmentLoadOp::LOAD)
            .store_op(vk::AttachmentStoreOp::STORE)];
        let render_info = vk::RenderingInfo::default()
            .render_area(vk::Rect2D {
                extent: self.swapchain_extent,
                ..Default::default()
            })
            .layer_count(1)
            .color_attachments(&color_attachment);

        self.device.cmd_begin_rendering(command, &render_info);

        let ui = imgui_data.imgui_ctx.frame();
        self.build_ui(ui);
        imgui_data.imgui_platform.prepare_render(ui, &self.window);
        let draw_data = imgui_data.imgui_ctx.render();
        imgui_data.imgui_renderer.cmd_draw(command, draw_data)?;

        self.device.cmd_end_rendering(command);

        Ok(())
    }

    fn get_current_frame(&self) -> &FrameData {
        &self.frames[self.frame_number % FRAME_OVERLAP]
    }

    fn build_ui(&mut self, ui: &mut imgui::Ui) {
        ui.window("Background")
            .size([300.0, 200.0], Condition::Appearing)
            .build(|| {
                ui.slider("Render Scale", 0.3, 1.0, &mut self.render_scale);

                let effects_len = self.background_effects.len();
                let selected = &mut self.background_effects[self.current_background_effect];

                ui.text(format!("Selected effect: {}", selected.name));

                ui.slider(
                    "Effect Index",
                    0,
                    effects_len - 1,
                    &mut self.current_background_effect,
                );

                let mut data1 = vec4_to_array(selected.data.data1);
                let mut data2 = vec4_to_array(selected.data.data2);
                let mut data3 = vec4_to_array(selected.data.data3);
                let mut data4 = vec4_to_array(selected.data.data4);
                ui.input_float4("Data 1", &mut data1).build();
                ui.input_float4("Data 2", &mut data2).build();
                ui.input_float4("Data 3", &mut data3).build();
                ui.input_float4("Data 4", &mut data4).build();

                selected.data.data1 = array_to_vec4(data1);
                selected.data.data2 = array_to_vec4(data2);
                selected.data.data3 = array_to_vec4(data3);
                selected.data.data4 = array_to_vec4(data4);
            });
    }

    unsafe fn draw(&mut self, _delta_time: Duration) -> Result<()> {
        self.device.wait_for_fences(
            &[self.get_current_frame().render_fence],
            true,
            ONE_SECOND_IN_NANO_SEC,
        )?;
        self.device
            .reset_fences(&[self.get_current_frame().render_fence])?;

        // TODO: Delete things for current frame

        self.frames[self.frame_number % FRAME_OVERLAP]
            .frame_descriptors
            .clear_pools(&self.device)?;

        let result = self.swapchain_extension.acquire_next_image(
            self.swapchain,
            ONE_SECOND_IN_NANO_SEC,
            self.get_current_frame().swapchain_semaphore,
            vk::Fence::null(),
        );

        let image_index = match result {
            Ok((image_index, _)) => image_index as usize,
            Err(vk::Result::ERROR_OUT_OF_DATE_KHR) => {
                self.resize_requested = true;
                return Ok(());
            }
            Err(e) => return Err(anyhow!(e)),
        };

        let command = self.get_current_frame().command_buffer;
        self.device
            .reset_command_buffer(command, vk::CommandBufferResetFlags::empty())?;

        let command_begin_info = vk::CommandBufferBeginInfo::default()
            .flags(vk::CommandBufferUsageFlags::ONE_TIME_SUBMIT);

        self.draw_extent.height = (self
            .swapchain_extent
            .height
            .min(self.draw_image.image_extent.height) as f32
            * self.render_scale) as u32;
        self.draw_extent.width = (self
            .swapchain_extent
            .width
            .min(self.draw_image.image_extent.width) as f32
            * self.render_scale) as u32;

        self.device
            .begin_command_buffer(command, &command_begin_info)?;

        transition_image_layout(
            &self.device,
            command,
            self.draw_image.image,
            vk::ImageLayout::UNDEFINED,
            vk::ImageLayout::GENERAL,
        )?;

        self.draw_background(command);

        transition_image_layout(
            &self.device,
            command,
            self.draw_image.image,
            vk::ImageLayout::GENERAL,
            vk::ImageLayout::TRANSFER_SRC_OPTIMAL,
        )?;

        transition_image_layout(
            &self.device,
            command,
            self.swapchain_images[image_index],
            vk::ImageLayout::UNDEFINED,
            vk::ImageLayout::TRANSFER_DST_OPTIMAL,
        )?;

        copy_image_to_image(
            &self.device,
            command,
            self.draw_image.image,
            self.swapchain_images[image_index],
            self.draw_extent,
            self.swapchain_extent,
        );

        transition_image_layout(
            &self.device,
            command,
            self.swapchain_images[image_index],
            vk::ImageLayout::TRANSFER_DST_OPTIMAL,
            vk::ImageLayout::COLOR_ATTACHMENT_OPTIMAL,
        )?;

        {
            let mut imgui_data = self.imgui_data.take().unwrap();

            self.draw_imgui(
                command,
                self.swapchain_image_views[image_index],
                &mut imgui_data,
            )?;

            _ = self.imgui_data.insert(imgui_data);
        }

        transition_image_layout(
            &self.device,
            command,
            self.swapchain_images[image_index],
            vk::ImageLayout::COLOR_ATTACHMENT_OPTIMAL,
            vk::ImageLayout::PRESENT_SRC_KHR,
        )?;

        self.device.end_command_buffer(command)?;

        let command_info = vk::CommandBufferSubmitInfo::default().command_buffer(command);

        let wait_info = vk::SemaphoreSubmitInfo::default()
            .stage_mask(vk::PipelineStageFlags2::COLOR_ATTACHMENT_OUTPUT)
            .semaphore(self.get_current_frame().swapchain_semaphore)
            .value(1);
        let signal_info = vk::SemaphoreSubmitInfo::default()
            .stage_mask(vk::PipelineStageFlags2::ALL_GRAPHICS)
            .semaphore(self.get_current_frame().render_semaphore)
            .value(1);

        let command_infos = &[command_info];
        let wait_infos = &[wait_info];
        let signal_infos = &[signal_info];

        let submit_info = vk::SubmitInfo2::default()
            .wait_semaphore_infos(wait_infos)
            .signal_semaphore_infos(signal_infos)
            .command_buffer_infos(command_infos);

        let submit_infos = &[submit_info];
        self.device.queue_submit2(
            self.graphics_queue,
            submit_infos,
            self.get_current_frame().render_fence,
        )?;

        let swapchains = &[self.swapchain];
        let image_indices = &[image_index as u32];
        let wait_semaphores = &[self.get_current_frame().render_semaphore];
        let present_info = vk::PresentInfoKHR::default()
            .swapchains(swapchains)
            .wait_semaphores(wait_semaphores)
            .image_indices(image_indices);

        let result = self
            .swapchain_extension
            .queue_present(self.graphics_queue, &present_info);

        match result {
            Err(vk::Result::ERROR_OUT_OF_DATE_KHR) => {
                self.resize_requested = true;
            }
            Err(error) => {
                return Err(anyhow!(error));
            }
            _ => {}
        }

        self.frame_number += 1;

        Ok(())
    }

    unsafe fn draw_background(&self, command: vk::CommandBuffer) {
        let effect = &self.background_effects[self.current_background_effect];
        let mut effect_data_copy = effect.data;

        self.device
            .cmd_bind_pipeline(command, vk::PipelineBindPoint::COMPUTE, effect.pipeline);
        let sets = &[self.draw_image_descriptors];
        self.device.cmd_bind_descriptor_sets(
            command,
            vk::PipelineBindPoint::COMPUTE,
            self.gradient_pipeline_layout,
            0,
            sets,
            &[],
        );

        // Write the resolution in the last bytes of the push constants
        effect_data_copy.data4.z = self.window_extent.width as f32 * self.render_scale;
        effect_data_copy.data4.w = self.window_extent.height as f32 * self.render_scale;

        let push_bytes = bytemuck::bytes_of(&effect_data_copy);

        self.device.cmd_push_constants(
            command,
            self.gradient_pipeline_layout,
            vk::ShaderStageFlags::COMPUTE,
            0,
            push_bytes,
        );

        self.device.cmd_dispatch(
            command,
            f32::ceil(self.draw_extent.width as f32 / 16.0) as u32,
            f32::ceil(self.draw_extent.height as f32 / 16.0) as u32,
            1,
        );
    }

    unsafe fn immediate_submit(
        &self,
        mut function: impl FnMut(vk::CommandBuffer) -> Result<()>,
    ) -> Result<()> {
        let fences = &[self.immediate_fence];
        self.device.reset_fences(fences)?;
        self.device.reset_command_buffer(
            self.immediate_command_buffer,
            vk::CommandBufferResetFlags::empty(),
        )?;

        let cmd = self.immediate_command_buffer;
        let cmd_begin_info = vk::CommandBufferBeginInfo::default()
            .flags(vk::CommandBufferUsageFlags::ONE_TIME_SUBMIT);

        self.device.begin_command_buffer(cmd, &cmd_begin_info)?;

        function(cmd)?;

        self.device.end_command_buffer(cmd)?;

        let submit_info = vk::CommandBufferSubmitInfo::default().command_buffer(cmd);
        let infos = &[submit_info];
        let submit = vk::SubmitInfo2::default().command_buffer_infos(infos);

        let submits = &[submit];
        self.device
            .queue_submit2(self.graphics_queue, submits, self.immediate_fence)?;

        let fences = &[self.immediate_fence];
        self.device.wait_for_fences(fences, true, 99999999999)?;

        Ok(())
    }

    pub unsafe fn resize_swapchain(&mut self) -> Result<()> {
        self.device.device_wait_idle()?;

        self.destroy_swapchain();

        // Convert "real" (winit) window size to vulkan window extent
        self.window_extent.width = self.window_size.width;
        self.window_extent.height = self.window_size.height;

        let create_data = GraphicData::create_swapchain(
            &self.instance,
            &self.device,
            &self.window,
            self.physical_device,
            &self.surface_extension,
            self.surface,
            Some(self.window_extent),
        )?;

        self.swapchain_extension = create_data.swapchain_extension;
        self.swapchain = create_data.swapchain;
        self.swapchain_extent = create_data.swapchain_extent;
        self.swapchain_images = create_data.swapchain_images;
        self.swapchain_image_format = create_data.swapchain_format;
        self.swapchain_image_views = create_data.swapchain_image_views;

        self.resize_requested = false;
        Ok(())
    }

    unsafe fn create_buffer(
        &mut self,
        allocation_size: vk::DeviceSize,
        usage: vk::BufferUsageFlags,
        memory_usage: vk_mem::MemoryUsage,
    ) -> Result<AllocatedBuffer> {
        let buffer_info = vk::BufferCreateInfo::default()
            .size(allocation_size)
            .usage(usage);

        let vma_allocation_info = vk_mem::AllocationCreateInfo {
            usage: memory_usage,
            flags: vk_mem::AllocationCreateFlags::MAPPED
                | vk_mem::AllocationCreateFlags::HOST_ACCESS_SEQUENTIAL_WRITE,
            ..Default::default()
        };

        let (buffer, allocation) = self
            .gpu_allocator
            .lock()
            .unwrap()
            .create_buffer(&buffer_info, &vma_allocation_info)?;

        let allocation_info = self
            .gpu_allocator
            .lock()
            .unwrap()
            .get_allocation_info(&allocation);

        Ok(AllocatedBuffer {
            buffer,
            allocation,
            allocation_info,
        })
    }

    unsafe fn destroy_buffer(&mut self, buffer: &mut AllocatedBuffer) {
        self.gpu_allocator
            .lock()
            .unwrap()
            .destroy_buffer(buffer.buffer, &mut buffer.allocation);
    }

    pub unsafe fn create_image(
        &mut self,
        extent: vk::Extent3D,
        format: vk::Format,
        usage: vk::ImageUsageFlags,
        mipmapped: bool,
    ) -> Result<AllocatedImage> {
        let mut image_create_info = vk::ImageCreateInfo::default()
            .format(format)
            .usage(usage)
            .extent(extent)
            .mip_levels(1)
            .array_layers(1)
            .image_type(vk::ImageType::TYPE_2D)
            .samples(vk::SampleCountFlags::TYPE_1)
            .tiling(vk::ImageTiling::OPTIMAL);

        if mipmapped {
            image_create_info.mip_levels =
                f32::floor(f32::log2(u32::max(extent.width, extent.height) as f32)) as u32;
        }

        let allocation_info = vk_mem::AllocationCreateInfo {
            usage: vk_mem::MemoryUsage::Auto,
            required_flags: vk::MemoryPropertyFlags::DEVICE_LOCAL,
            ..Default::default()
        };

        let (image, allocation) = self
            .gpu_allocator
            .lock()
            .expect("Could not get lock on GPU allocator.")
            .create_image(&image_create_info, &allocation_info)
            .expect("Could not create the image");

        let aspect_flags = if format == vk::Format::D32_SFLOAT {
            vk::ImageAspectFlags::DEPTH
        } else {
            vk::ImageAspectFlags::COLOR
        };

        let subresource = vk::ImageSubresourceRange::default()
            .level_count(image_create_info.mip_levels)
            .layer_count(1)
            .aspect_mask(aspect_flags);
        let view_create_info = vk::ImageViewCreateInfo::default()
            .format(format)
            .image(image)
            .view_type(vk::ImageViewType::TYPE_2D)
            .subresource_range(subresource);

        let image_view = self.device.create_image_view(&view_create_info, None)?;

        Ok(AllocatedImage {
            image_extent: extent,
            image,
            image_view,
            image_format: format,
            gpu_allocation: allocation,
        })
    }

    pub unsafe fn create_image_from_bytes(
        &mut self,
        data: &[u8],
        extent: vk::Extent3D,
        format: vk::Format,
        usage: vk::ImageUsageFlags,
        mipmapped: bool,
    ) -> Result<AllocatedImage> {
        const BYTES_PER_PIXEL: vk::DeviceSize = 4;
        let data_size = extent.depth as vk::DeviceSize
            * extent.width as vk::DeviceSize
            * extent.height as vk::DeviceSize
            * BYTES_PER_PIXEL;

        let mut upload_buffer = self.create_buffer(
            data_size,
            vk::BufferUsageFlags::TRANSFER_SRC,
            vk_mem::MemoryUsage::Auto,
        )?;
        let mapped_upload_buffer = self
            .gpu_allocator
            .lock()
            .unwrap()
            .map_memory(&mut upload_buffer.allocation)?;

        std::ptr::copy_nonoverlapping(data.as_ptr(), mapped_upload_buffer, data_size as usize);

        self.gpu_allocator
            .lock()
            .unwrap()
            .unmap_memory(&mut upload_buffer.allocation);

        let new_image = self.create_image(
            extent,
            format,
            usage | vk::ImageUsageFlags::TRANSFER_DST | vk::ImageUsageFlags::TRANSFER_SRC,
            mipmapped,
        )?;

        self.immediate_submit(|command| {
            transition_image_layout(
                &self.device,
                command,
                new_image.image,
                vk::ImageLayout::UNDEFINED,
                vk::ImageLayout::TRANSFER_DST_OPTIMAL,
            )?;

            let subresource = vk::ImageSubresourceLayers::default()
                .aspect_mask(vk::ImageAspectFlags::COLOR)
                .layer_count(1);

            let copy_region = vk::BufferImageCopy::default()
                .image_extent(extent)
                .image_subresource(subresource);

            self.device.cmd_copy_buffer_to_image(
                command,
                upload_buffer.buffer,
                new_image.image,
                vk::ImageLayout::TRANSFER_DST_OPTIMAL,
                std::slice::from_ref(&copy_region),
            );

            transition_image_layout(
                &self.device,
                command,
                new_image.image,
                vk::ImageLayout::TRANSFER_DST_OPTIMAL,
                vk::ImageLayout::SHADER_READ_ONLY_OPTIMAL,
            )?;

            Ok(())
        })?;

        self.destroy_buffer(&mut upload_buffer);

        Ok(new_image)
    }

    pub unsafe fn destroy_image(&mut self, allocated_image: &mut AllocatedImage) {
        self.device
            .destroy_image_view(allocated_image.image_view, None);
        self.gpu_allocator
            .lock()
            .unwrap()
            .destroy_image(allocated_image.image, &mut allocated_image.gpu_allocation);
    }
}

struct VulkanInitializationData {
    entry: ash::Entry,
    instance: ash::Instance,
    debug_extension: ash::ext::debug_utils::Instance,
    debug_messenger: vk::DebugUtilsMessengerEXT,
}

struct DescriptorsInitializationData {
    global_descriptor_allocator: DescriptorAllocator,
    draw_image_descriptors: vk::DescriptorSet,
    draw_image_descriptor_layout: vk::DescriptorSetLayout,
    single_image_descriptor_layout: vk::DescriptorSetLayout,
}

pub struct CreateSwapchainInfo<'a> {
    pub window: &'a Window,
    pub instance: &'a ash::Instance,
    pub device: &'a ash::Device,
    pub surface_extension: &'a ash::khr::surface::Instance,
    pub surface: vk::SurfaceKHR,
    pub physical_device: vk::PhysicalDevice,
    pub gpu_allocator: Arc<Mutex<vk_mem::Allocator>>,
}

pub struct SwapchainCreationData {
    pub swapchain_format: vk::Format,
    pub swapchain_extent: vk::Extent2D,
    pub swapchain: vk::SwapchainKHR,
    pub swapchain_image_views: Vec<vk::ImageView>,
    pub swapchain_images: Vec<vk::Image>,
    pub swapchain_extension: ash::khr::swapchain::Device,
}

pub struct SwapchainInitCreationData {
    pub swapchain_format: vk::Format,
    pub swapchain_extent: vk::Extent2D,
    pub swapchain: vk::SwapchainKHR,
    pub swapchain_images: Vec<vk::Image>,
    pub swapchain_extension: ash::khr::swapchain::Device,
    pub swapchain_image_views: Vec<vk::ImageView>,
    pub draw_image: AllocatedImage,
    pub depth_image: AllocatedImage,
}

impl ApplicationHandler for VulkanEngine {
    fn resumed(&mut self, event_loop: &ActiveEventLoop) {
        self.graphic_data = Some(GraphicData::load(event_loop).unwrap())
    }

    fn window_event(
        &mut self,
        event_loop: &ActiveEventLoop,
        window_id: WindowId,
        event: WindowEvent,
    ) {
        let graphic_data = self.graphic_data.as_mut().unwrap();
        let imgui_data = graphic_data.imgui_data.as_mut().unwrap();

        if window_id != graphic_data.window.id() {
            return;
        }

        imgui_data.imgui_platform.handle_window_event(
            imgui_data.imgui_ctx.io_mut(),
            &graphic_data.window,
            &event,
        );

        match event {
            WindowEvent::CloseRequested => {
                graphic_data.should_stop_rendering = true;
                event_loop.exit();
                info!("Closing window...");
            }
            WindowEvent::Resized(size) => {
                graphic_data.window_size = size;
                if size.width == 0 || size.height == 0 {
                    graphic_data.minimized = true;
                } else {
                    graphic_data.minimized = false;
                    graphic_data.resize_requested = true;
                }
            }
            WindowEvent::RedrawRequested => {
                let now = Instant::now();
                let delta_time = now - graphic_data.last_time;
                graphic_data.last_time = now;

                imgui_data.imgui_ctx.io_mut().update_delta_time(delta_time);

                unsafe {
                    graphic_data
                        .draw(delta_time)
                        .expect("Unexpected error in draw loop.")
                };

                if graphic_data.resize_requested {
                    unsafe {
                        graphic_data
                            .resize_swapchain()
                            .expect("Unexpected error in the resize of the swapchain.")
                    };
                }
            }
            _ => {}
        }
    }

    fn about_to_wait(&mut self, _: &ActiveEventLoop) {
        let graphic_data = self.graphic_data.as_mut().unwrap();
        let imgui_data = graphic_data.imgui_data.as_mut().unwrap();

        if !graphic_data.should_stop_rendering && !graphic_data.minimized {
            imgui_data
                .imgui_platform
                .prepare_frame(imgui_data.imgui_ctx.io_mut(), &graphic_data.window)
                .expect("Failed to prepare imgui frame");
            graphic_data.window.request_redraw();
        }
    }
}

impl Drop for GraphicData {
    fn drop(&mut self) {
        unsafe {
            self.device.device_wait_idle().unwrap();

            self.device
                .destroy_command_pool(self.immediate_command_pool, None);

            self.device
                .destroy_descriptor_set_layout(self.draw_image_descriptor_layout, None);
            self.device
                .destroy_descriptor_set_layout(self.single_image_descriptor_layout, None);
            self.global_descriptor_allocator.destroy_pool(&self.device);

            let imgui_data = self.imgui_data.take().unwrap();
            drop(imgui_data);

            // del imm fence
            self.device.destroy_fence(self.immediate_fence, None);

            for frame in &mut self.frames {
                self.device.destroy_command_pool(frame.command_pool, None);
                frame.frame_descriptors.destroy_pools(&self.device);

                self.device.destroy_fence(frame.render_fence, None);
                self.device.destroy_semaphore(frame.render_semaphore, None);
                self.device
                    .destroy_semaphore(frame.swapchain_semaphore, None);
            }

            self.device
                .destroy_pipeline_layout(self.gradient_pipeline_layout, None);

            self.background_effects
                .iter()
                .for_each(|e| self.device.destroy_pipeline(e.pipeline, None));

            self.destroy_swapchain();

            self.device
                .destroy_image_view(self.depth_image.image_view, None);
            self.gpu_allocator
                .lock()
                .unwrap()
                .destroy_image(self.depth_image.image, &mut self.depth_image.gpu_allocation);

            self.device
                .destroy_image_view(self.draw_image.image_view, None);
            self.gpu_allocator
                .lock()
                .unwrap()
                .destroy_image(self.draw_image.image, &mut self.draw_image.gpu_allocation);

            // Destroy allocator
            ManuallyDrop::drop(&mut self.gpu_allocator);

            self.surface_extension.destroy_surface(self.surface, None);

            self.device.destroy_device(None);

            if VALIDATION_ENABLED {
                self.debug_extension
                    .destroy_debug_utils_messenger(self.debug_messenger, None);
            }

            self.instance.destroy_instance(None);
        }
    }
}
